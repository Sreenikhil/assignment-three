// I am gonna do a crazy job
// Build an express app which has a timestamp middleware and Logging middleware

import {NextFunction, Request, Response, Application} from 'express'
import mongoose = require('mongoose');
const Retailer = require('./Models/retailerSchema').Retailer;
import Express = require('express');
import {customMiddleware} from './Middlewares/cookie_middleware'
import {strongMiddleware} from './Middlewares/cookie_middleware'
const Sessions = require('./Models/SessionSchema').Sessions;
import cookieParser = require('cookie-parser');
import nanoid = require('nanoid');
import bcrypt = require('bcrypt');
const dotenv = require('dotenv').config();
const CrossReference = require('./Models/CrossReferenceSchema').CrossReference
const Medicine = require('./Models/medicineSchema').Medicine


const app: Application = Express();
app.use(cookieParser(process.env.secrete));
app.use(Express.json());




const db_connect =  mongoose.connect("mongodb://localhost:27017/testdb", {useNewUrlParser: true, useUnifiedTopology: true });
//
// -----------------------------   Inserting Data ------------------------------------------
// To insert all retailers in to the database
// Make sure that all post middleware passes through strong middleware
app.post("/retailer-reg",   [strongMiddleware({Company_name: 'string', Country_code: 'number', Phone_Number: 'number', Address: 'string', Country: 'string', State: 'string', Retailer_ID: 'string', Password: 'string'})], async (request: Request, response: Response) => {
    console.log("I am in retailer registration");
    // Let me check for unique company name and retailer id
    const existsfalg = await Retailer.findOne( {$or: [
        {Company_name: {$eq: response.locals.strongParams['Company_name']}},
            {Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID']}}
            ]});

    console.log("Existing retailer is: "+ existsfalg);
    if(existsfalg){
        return response.send("Duplicate retailer").status(200)
    }
    else {
        try {
            await bcrypt.hash(response.locals.strongParams['Password'], 12, async (err, hash) => {
                if (err) {
                    console.log("Problem in doing hash" + hash);
                    return response.sendStatus(404)
                } else {
                    try {
                        await Retailer.create({
                            Company_name: response.locals.strongParams['Company_name'],
                            Country_code: response.locals.strongParams['Country_code'],
                            Phone_Number: response.locals.strongParams['Phone_Number'],
                            Address: response.locals.strongParams['Address'],
                            Country: response.locals.strongParams['Country'],
                            City: response.locals.strongParams['State'],
                            Retailer_ID: response.locals.strongParams['Retailer_ID'],
                            Password: hash
                        });
                    }
                    catch(err){
                        console.log("Error in making the request")
                        const msge = {msg: "Invalid request"}
                        return response.send(msge).status(200)
                    }
                    return response.sendStatus(200)
                }
            });
        } catch (err) {
            console.log("Error performing the request" + err.message)
        }
    }
 });

// customMiddleware({requireCookie: false})
// --------------------------   Handling session - Login Route  ------------------------------------
// Our job is to check if we can allow the user
// If Secure is made false I got a cookie
// Write code so that, if more than 5 sessions exists then remove one of them
app.post("/session", [
    customMiddleware({requireCookie: false}),
    strongMiddleware({Retailer_ID: 'string', Password: 'string'})
   ] , async (request: Request, response: Response) =>{
    console.log("The cookie end point is hit");
    // If user cookie exixts do not ask for details and retrive corresponding user
    if(response.locals.userCookie) {
        console.log("I found a cookie in login route");
        console.log("I am in the main middleware");
        console.log(response.locals.userCookie);
        const retailer = await Sessions.findOne({Session_ID: {$eq: response.locals.userCookie}});
        if (retailer) {
            console.log("Retailer Exists");
            console.log("Concerned retailer is: " + retailer.Company_name);
            return response.send("You did a great login job").status(200)
        }
    }
        console.log("I am checking for username and password");
        // Logic to check if login is successful
        const retailer = await Retailer.findOne({Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID']}});
        if (!retailer) {
            console.log("Invalid username/password");
            const msge = {msg: "Invalid request"}
            response.send(msge).status(200)
        }
        await bcrypt.compare(response.locals.strongParams['Password'], retailer.Password, async (err, result) => {
            if (err) console.log("There is an error checking password" + err.message);
            if (result) {
                console.log("I have successfully authenticated, now I am creating a session for the user");
                // store session id in the database
                let sessionid: string = nanoid();
                // I am omitting collisions because they occur 1 in 1 million
                console.log(sessionid);
                console.log("I have created a cookie for you ...");
                const num_of_sessions = await Sessions.find({Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID']}});
                console.log("Number of sessions are: "+ num_of_sessions.length);
                // I observed that it is deleting oldest session, since new records are being pushed as if they are in Queue (FCFS Order)
                if(num_of_sessions.length >=5){
                    console.log("I am deleting one of the session........");
                    await Sessions.findOneAndDelete({Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID'] }})
                }
                const deleted_session = await Sessions.findOne( {$and: [{Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID']}}, {Session_ID: {$eq: sessionid}}] } );
                if(deleted_session){
                    await Sessions.updateOne({$and: [{Retailer_ID: response.locals.strongParams['Retailer_ID']}, {Session_ID: {$eq: null}}]}, {Session_ID: sessionid});
                }
                else {
                    await Sessions.create({Session_ID: sessionid, Retailer_ID: response.locals.strongParams['Retailer_ID']});
                }
                console.log("You now have a signed cookie");
                response.cookie("userCookie", sessionid, {signed: true, secure: false, httpOnly: true});
                response.send('You now have a cookie called userCookie!');
            }
            else{
                console.log("The credentials did not match");
                const msge = {msg: "Invalid request"}
                response.send(msge).status(200)
            }
        });
});

// --------------------------   Retrieving session ------------------------------------
// Check if the cookie sent is there in database
app.get('/retailer_home', [customMiddleware({requireCookie: true})], (request: Request, response: Response) =>{
    console.log("The user cookie is: "+ response.locals.userCookie);
    // Check if
    console.log("I am the homepage");
    return response.send("Good job! you are now in homepage")
});


// Route to log out of the session
// My log out is that I am completely wiping out session by making session id as null

// --------------------------   Deleting session ------------------------------------
app.get('/logout', [customMiddleware({requireCookie: true})], async (request: Request, response: Response) =>{
    const k = await Sessions.findOne({Session_ID: {$eq: response.locals.userCookie}});
     k.Session_ID = null;
    await k.save();
    console.log("I am in logout root")
    response.clearCookie('userCookie');
    const msgobj = {msge: "You did a great logout"}
    return response.send(msgobj).status(200)
});


app.get("/allMedicines", [customMiddleware({requireCookie: true})], async(req:Request, res:Response) =>{
    console.log("I am in this path")
    const values = await CrossReference.find({})
    console.log("Values are: "+ values)
})


// -------------------


app.post('/addcross', [customMiddleware({requireCookie: true}), strongMiddleware(
    {Retailer: 'string', Medicine_ID: 'number', count: 'number', Medicine_name: 'string'})],  async(req: Request, res: Response) =>{
    console.log("Retailer ID is: "+res.locals.strongParams['Retailer'])
    console.log("Medicine ID is: "+ res.locals.strongParams['Medicine_ID'])
    console.log("Count is: "+ res.locals.strongParams['count'])
    console.log("Medicine Name is: "+res.locals.strongParams['Medicine_name'])

    console.log("I am in this path")
    const result = await CrossReference.findOne({Retailer_ID: {$eq: res.locals.strongParams['Retailer']}})
    if(!result) {
        try {
            await CrossReference.create({
                Retailer_ID: res.locals.strongParams['Retailer'],
                ID: [{Medicine_ID: res.locals.strongParams['Medicine_ID'], count: res.locals.strongParams['count'], Medicine_name: res.locals.strongParams['Medicine_name']}]
            })
            return res.sendStatus(200)
        } catch (error) {
            return res.sendStatus(400)
        }
    }
    else{
        try {
            console.log("I am updating")
            await CrossReference.update({Retailer_ID: res.locals.strongParams['Retailer']}, {
                $push: {
                    ID: [{
                        Medicine_ID: res.locals.strongParams['Medicine_ID'],
                        count: res.locals.strongParams['count'],
                        Medicine_name: res.locals.strongParams['Medicine_name']
                    }]
                }
            })
            console.log("I am after updating")
            return res.sendStatus(200)
        }
        catch(err){
            return res.sendStatus(400)
        }
    }
})
// --------------- Update Count -----------------------------

app.post('/updateMedicine',[customMiddleware({requireCookie: true})],  async (req: Request, res: Response)=>{
    const {Retailer_ID, Medicine_ID, value} = req.body
    console.log("The Retailer ID is:"+ Retailer_ID)
    console.log("The Medicine_ID is: "+ Medicine_ID)
    if(value === 1) {
        await CrossReference.updateOne({
            Retailer_ID: Retailer_ID,
            "ID.Medicine_ID": Medicine_ID
        }, {$inc: {"ID.$.count": 1}})
    }
    else{
        await CrossReference.updateOne({
            Retailer_ID: Retailer_ID,
            "ID.Medicine_ID": Medicine_ID
        }, {$inc: {"ID.$.count": -1}})
    }
    console.log("I have successfully incremented")
    return res.sendStatus(200)
})

// ----------- route to check if session exists -----------------

app.get('/session-exists', [customMiddleware({requireCookie: true})], async(request: Request, response: Response) =>{
    if(response.locals.userCookie){
        return response.sendStatus(200)
    }
})

// ------------------ Deleting a Medicine ----------------------

app.post('/delete', async(req: Request, res: Response) =>{
    console.log("I am in the delete request")
    const {Retailer_ID, Medicine_ID} = req.body
    console.log("The retailer ID is: "+ Retailer_ID)
    console.log("The medicine ID is: + "+ Medicine_ID)
    try{
        await CrossReference.updateOne({ Retailer_ID: Retailer_ID},
            {$pull:{ID: {Medicine_ID: Medicine_ID}} }
            )
        return res.sendStatus(200)
    }
    catch(err){
        console.log(err)
    }
})
// -------------- Code to get company name from retailer ID ----------------

app.get('/Company/:retailerId',[customMiddleware({requireCookie: true})], async (req: Request, res: Response) =>{
    console.log("I am in this end point")
    const name = req.params.retailerId
    console.log(name)
    try {
        const Company = await Retailer.findOne({Retailer_ID: {$eq: name}}, {Company_name: 1, _id:0})
        const obj = {Company_name: Company.Company_name}
        console.log("The name of the company is: "+ Company.Company_name)
        return res.send(obj).status(200)
    }
    catch(err){
        console.log("Could not fetch company: "+ err)
    }
})

//  ------------ Code to check if Medicine Exists ------------------------

app.get("/Medicine/:id/:retailerId",[customMiddleware({requireCookie: true})],  async (req: Request, res: Response) =>{
    console.log("I am checking if medicine exists")
    const id = req.params.id
    const retailerId = req.params.retailerId
    console.log("ID is: "+ id)
    console.log("Retailer id is: "+ retailerId)
    try{
        const response = await CrossReference.findOne({$and: [{Retailer_ID: {$eq: retailerId} }, {"ID":{$elemMatch: {Medicine_ID: id}}}] })

        console.log(response)
        if(response){
            return  res.sendStatus(200)
        }
        else{
            const obj = {msge: "Not Found"}
            return  res.send(obj).status(200)
        }
    }
    catch(err){
        console.log("Error in fetching medicine from the databse: "+ err)
    }
})






app.listen(5000, ()=>{console.log("I am listening to the port 5000")});

// Pending work is to code 6 unit test cases to check functioning of each middlewares. Together 12 test cases.
// Now I am able insert data, however I want. I should be able to train the graph ql
// Keep in mind of N+1 Problem


