import  { Schema} from 'mongoose'
import mongoose = require('mongoose');
import  {ICrossReference} from './CrossReference'

import {Medicine} from "./medicineSchema";
import {medicineSchema} from "./medicineSchema";
// I want to make, medicine ID a number which can at most contain 7 digits, making the largest medicine id possible is 9999999.
// There are atmost 1 million unique medicine ID's possible.

const crossReferenceSchema : mongoose.Schema<ICrossReference> = new mongoose.Schema<ICrossReference>({
    Retailer_ID: {
        type: String,
        required: true,  // Built-In validation
    },

    ID: {
        type: [medicineSchema]
    }

});

export const CrossReference = mongoose.model<ICrossReference>('CrossReference', crossReferenceSchema);