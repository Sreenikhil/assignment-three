import mongoose = require('mongoose');

export interface IMedicineResponse  extends mongoose.Document{
    Duration_in_days: number,
    Cost: number,
    Note: string
    MedicineID: number,
    RetailerID: string,
    ManufacturerID: string,
    by: string
}