"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var MedicineResponseSchema = new mongoose.Schema({
    Duration_in_days: {
        type: Number,
        required: true
    },
    Cost: {
        type: Number,
        required: true // Built-In validation
    },
    Note: {
        type: String,
        required: true // Built-In validation
    },
    MedicineID: {
        type: Number,
        required: true
    },
    RetailerID: {
        type: String,
        required: true
    },
    ManufacturerID: {
        type: String,
        required: true
    },
    by: {
        type: String,
        required: true
    }
});
exports.MedicineResponse = mongoose.model('Response', MedicineResponseSchema);
