"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
// I want to make, medicine ID a number which can at most contain 7 digits, making the largest medicine id possible is 9999999.
// There are atmost 1 million unique medicine ID's possible.
exports.medicineSchema = new mongoose.Schema({
    Medicine_name: {
        type: String,
        validate: {
            validator: function (medname) {
                return medname.length > 1;
            },
            message: "Medicine name should have length greater than 1"
        },
        required: true
    },
    count: {
        type: Number,
        required: true,
        validate: {
            validator: function (count) {
                return count > 0;
            },
            message: "Count should be greater than 0"
        }
    },
    Medicine_ID: {
        type: Number,
        validate: {
            validator: function (medid) {
                return medid > 0 && medid < 9999999;
            }
        },
        required: true
    }
});
exports.Medicine = mongoose.model('Medicine', exports.medicineSchema);
