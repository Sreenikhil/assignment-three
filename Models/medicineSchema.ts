
import {Document, Schema, Model, model} from 'mongoose'
import mongoose = require('mongoose');
import  {IMedicine} from './Medicine'

// I want to make, medicine ID a number which can at most contain 7 digits, making the largest medicine id possible is 9999999.
// There are atmost 1 million unique medicine ID's possible.

export const medicineSchema: mongoose.Schema<IMedicine> = new mongoose.Schema<IMedicine>({
        Medicine_name:{
            type: String,
            validate:{
                validator: function (medname: string): boolean {
                    return medname.length > 1;
                },
                message: "Medicine name should have length greater than 1"
            },
            required: true,  // Built-In validation
        },
        count:{
            type: Number,
            required: true,
            validate:{
                validator: function (count: number): boolean {
                    return count > 0;
                },
                message: "Count should be greater than 0"
            },
        },
       Medicine_ID:{
            type: Number,
            validate:{
                validator: function (medid: number): boolean{
                    return medid > 0 && medid < 9999999
                }
            },
            required: true
       }
    });

export const Medicine = mongoose.model<IMedicine>('Medicine', medicineSchema);