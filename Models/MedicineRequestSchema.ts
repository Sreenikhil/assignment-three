import mongoose = require('mongoose');
import {IMedicineRequest} from "./MedicineRequest";

const MedicineRequestSchema  : mongoose.Schema<IMedicineRequest> = new mongoose.Schema<IMedicineRequest>({


    MedicineCount:{
        type: Number,
        required: true  // Built-In validation
    },

    MedicineID:{
        type: Number,
        required: true
    },

    RetailerID:{
        type: String,
        required: true
    },

    Company_name:{
        type: String,
        required: true
    },
    Notes:{
        type: String,
        required: true
    },

    by:{
        type: String,
        required: true
    }

})

export const MedicineRequest = mongoose.model<IMedicineRequest>('Request', MedicineRequestSchema);


