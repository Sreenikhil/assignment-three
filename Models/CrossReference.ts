import mongoose = require('mongoose');
import {Schema} from "mongoose";
import {IMedicine} from "./Medicine";

export interface ICrossReference extends mongoose.Document {
    Retailer_ID: string,
     ID: [IMedicine],
}