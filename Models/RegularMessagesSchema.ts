import mongoose = require('mongoose');
import {IRegularMessages} from "./RegularMessages"

const RegularMessagesSchema : mongoose.Schema<IRegularMessages> = new mongoose.Schema<IRegularMessages>({
    Source: {
        type: String,
        required: true,  // Built-In validation
    },

    Destination: {
        type: String,
        required: true  // Built-In validation
    },
    Message: {
        type: String,
        required: true  // Built-In validation
    }
})

export const RegularMessages = mongoose.model<IRegularMessages>('Messages', RegularMessagesSchema);
