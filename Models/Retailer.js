"use strict";
exports.__esModule = true;
// 5 associated unit tests
/*
   Test Password, if it contains length 3 - 16, has some special characters, uppercase, lowercase and a number.
   Test if retailer id is a string and not a number
   Test if city is a string
   Test if company name is a string
   Test if company name, retailer id and password is given or not in a typical usecase.
 */
