"use strict";
// I am gonna do a crazy job
// Build an express app which has a timestamp middleware and Logging middleware
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var Retailer = require('./Models/retailerSchema').Retailer;
var Express = require("express");
var cookie_middleware_1 = require("./Middlewares/cookie_middleware");
var cookie_middleware_2 = require("./Middlewares/cookie_middleware");
var Sessions = require('./Models/SessionSchema').Sessions;
var cookieParser = require("cookie-parser");
var nanoid = require("nanoid");
var bcrypt = require("bcrypt");
var dotenv = require('dotenv').config();
var CrossReference = require('./Models/CrossReferenceSchema').CrossReference;
var Medicine = require('./Models/medicineSchema').Medicine;
var app = Express();
app.use(cookieParser(process.env.secrete));
app.use(Express.json());
var db_connect = mongoose.connect("mongodb://localhost:27017/testdb", { useNewUrlParser: true, useUnifiedTopology: true });
//
// -----------------------------   Inserting Data ------------------------------------------
// To insert all retailers in to the database
// Make sure that all post middleware passes through strong middleware
app.post("/retailer-reg", [cookie_middleware_2.strongMiddleware({ Company_name: 'string', Country_code: 'number', Phone_Number: 'number', Address: 'string', Country: 'string', State: 'string', Retailer_ID: 'string', Password: 'string' })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var existsfalg, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("I am in retailer registration");
                return [4 /*yield*/, Retailer.findOne({ $or: [
                            { Company_name: { $eq: response.locals.strongParams['Company_name'] } },
                            { Retailer_ID: { $eq: response.locals.strongParams['Retailer_ID'] } }
                        ] })];
            case 1:
                existsfalg = _a.sent();
                console.log("Existing retailer is: " + existsfalg);
                if (!existsfalg) return [3 /*break*/, 2];
                return [2 /*return*/, response.send("Duplicate retailer").status(200)];
            case 2:
                _a.trys.push([2, 4, , 5]);
                return [4 /*yield*/, bcrypt.hash(response.locals.strongParams['Password'], 12, function (err, hash) { return __awaiter(void 0, void 0, void 0, function () {
                        var err_2, msge;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!err) return [3 /*break*/, 1];
                                    console.log("Problem in doing hash" + hash);
                                    return [2 /*return*/, response.sendStatus(404)];
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    return [4 /*yield*/, Retailer.create({
                                            Company_name: response.locals.strongParams['Company_name'],
                                            Country_code: response.locals.strongParams['Country_code'],
                                            Phone_Number: response.locals.strongParams['Phone_Number'],
                                            Address: response.locals.strongParams['Address'],
                                            Country: response.locals.strongParams['Country'],
                                            City: response.locals.strongParams['State'],
                                            Retailer_ID: response.locals.strongParams['Retailer_ID'],
                                            Password: hash
                                        })];
                                case 2:
                                    _a.sent();
                                    return [3 /*break*/, 4];
                                case 3:
                                    err_2 = _a.sent();
                                    console.log("Error in making the request");
                                    msge = { msg: "Invalid request" };
                                    return [2 /*return*/, response.send(msge).status(200)];
                                case 4: return [2 /*return*/, response.sendStatus(200)];
                            }
                        });
                    }); })];
            case 3:
                _a.sent();
                return [3 /*break*/, 5];
            case 4:
                err_1 = _a.sent();
                console.log("Error performing the request" + err_1.message);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
// customMiddleware({requireCookie: false})
// --------------------------   Handling session - Login Route  ------------------------------------
// Our job is to check if we can allow the user
// If Secure is made false I got a cookie
// Write code so that, if more than 5 sessions exists then remove one of them
app.post("/session", [
    cookie_middleware_1.customMiddleware({ requireCookie: false }),
    cookie_middleware_2.strongMiddleware({ Retailer_ID: 'string', Password: 'string' })
], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var retailer_1, retailer, msge;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("The cookie end point is hit");
                if (!response.locals.userCookie) return [3 /*break*/, 2];
                console.log("I found a cookie in login route");
                console.log("I am in the main middleware");
                console.log(response.locals.userCookie);
                return [4 /*yield*/, Sessions.findOne({ Session_ID: { $eq: response.locals.userCookie } })];
            case 1:
                retailer_1 = _a.sent();
                if (retailer_1) {
                    console.log("Retailer Exists");
                    console.log("Concerned retailer is: " + retailer_1.Company_name);
                    return [2 /*return*/, response.send("You did a great login job").status(200)];
                }
                _a.label = 2;
            case 2:
                console.log("I am checking for username and password");
                return [4 /*yield*/, Retailer.findOne({ Retailer_ID: { $eq: response.locals.strongParams['Retailer_ID'] } })];
            case 3:
                retailer = _a.sent();
                if (!retailer) {
                    console.log("Invalid username/password");
                    msge = { msg: "Invalid request" };
                    response.send(msge).status(200);
                }
                return [4 /*yield*/, bcrypt.compare(response.locals.strongParams['Password'], retailer.Password, function (err, result) { return __awaiter(void 0, void 0, void 0, function () {
                        var sessionid, num_of_sessions, deleted_session, msge;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (err)
                                        console.log("There is an error checking password" + err.message);
                                    if (!result) return [3 /*break*/, 9];
                                    console.log("I have successfully authenticated, now I am creating a session for the user");
                                    sessionid = nanoid();
                                    // I am omitting collisions because they occur 1 in 1 million
                                    console.log(sessionid);
                                    console.log("I have created a cookie for you ...");
                                    return [4 /*yield*/, Sessions.find({ Retailer_ID: { $eq: response.locals.strongParams['Retailer_ID'] } })];
                                case 1:
                                    num_of_sessions = _a.sent();
                                    console.log("Number of sessions are: " + num_of_sessions.length);
                                    if (!(num_of_sessions.length >= 5)) return [3 /*break*/, 3];
                                    console.log("I am deleting one of the session........");
                                    return [4 /*yield*/, Sessions.findOneAndDelete({ Retailer_ID: { $eq: response.locals.strongParams['Retailer_ID'] } })];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3: return [4 /*yield*/, Sessions.findOne({ $and: [{ Retailer_ID: { $eq: response.locals.strongParams['Retailer_ID'] } }, { Session_ID: { $eq: sessionid } }] })];
                                case 4:
                                    deleted_session = _a.sent();
                                    if (!deleted_session) return [3 /*break*/, 6];
                                    return [4 /*yield*/, Sessions.updateOne({ $and: [{ Retailer_ID: response.locals.strongParams['Retailer_ID'] }, { Session_ID: { $eq: null } }] }, { Session_ID: sessionid })];
                                case 5:
                                    _a.sent();
                                    return [3 /*break*/, 8];
                                case 6: return [4 /*yield*/, Sessions.create({ Session_ID: sessionid, Retailer_ID: response.locals.strongParams['Retailer_ID'] })];
                                case 7:
                                    _a.sent();
                                    _a.label = 8;
                                case 8:
                                    console.log("You now have a signed cookie");
                                    response.cookie("userCookie", sessionid, { signed: true, secure: false, httpOnly: true });
                                    response.send('You now have a cookie called userCookie!');
                                    return [3 /*break*/, 10];
                                case 9:
                                    console.log("The credentials did not match");
                                    msge = { msg: "Invalid request" };
                                    response.send(msge).status(200);
                                    _a.label = 10;
                                case 10: return [2 /*return*/];
                            }
                        });
                    }); })];
            case 4:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
// --------------------------   Retrieving session ------------------------------------
// Check if the cookie sent is there in database
app.get('/retailer_home', [cookie_middleware_1.customMiddleware({ requireCookie: true })], function (request, response) {
    console.log("The user cookie is: " + response.locals.userCookie);
    // Check if
    console.log("I am the homepage");
    return response.send("Good job! you are now in homepage");
});
// Route to log out of the session
// My log out is that I am completely wiping out session by making session id as null
// --------------------------   Deleting session ------------------------------------
app.get('/logout', [cookie_middleware_1.customMiddleware({ requireCookie: true })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var k, msgobj;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, Sessions.findOne({ Session_ID: { $eq: response.locals.userCookie } })];
            case 1:
                k = _a.sent();
                k.Session_ID = null;
                return [4 /*yield*/, k.save()];
            case 2:
                _a.sent();
                console.log("I am in logout root");
                response.clearCookie('userCookie');
                msgobj = { msge: "You did a great logout" };
                return [2 /*return*/, response.send(msgobj).status(200)];
        }
    });
}); });
app.get("/allMedicines", [cookie_middleware_1.customMiddleware({ requireCookie: true })], function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var values;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("I am in this path");
                return [4 /*yield*/, CrossReference.find({})];
            case 1:
                values = _a.sent();
                console.log("Values are: " + values);
                return [2 /*return*/];
        }
    });
}); });
// -------------------
app.post('/addcross', [cookie_middleware_1.customMiddleware({ requireCookie: true }), cookie_middleware_2.strongMiddleware({ Retailer: 'string', Medicine_ID: 'number', count: 'number', Medicine_name: 'string' })], function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var result, error_1, err_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("Retailer ID is: " + res.locals.strongParams['Retailer']);
                console.log("Medicine ID is: " + res.locals.strongParams['Medicine_ID']);
                console.log("Count is: " + res.locals.strongParams['count']);
                console.log("Medicine Name is: " + res.locals.strongParams['Medicine_name']);
                console.log("I am in this path");
                return [4 /*yield*/, CrossReference.findOne({ Retailer_ID: { $eq: res.locals.strongParams['Retailer'] } })];
            case 1:
                result = _a.sent();
                if (!!result) return [3 /*break*/, 6];
                _a.label = 2;
            case 2:
                _a.trys.push([2, 4, , 5]);
                return [4 /*yield*/, CrossReference.create({
                        Retailer_ID: res.locals.strongParams['Retailer'],
                        ID: [{ Medicine_ID: res.locals.strongParams['Medicine_ID'], count: res.locals.strongParams['count'], Medicine_name: res.locals.strongParams['Medicine_name'] }]
                    })];
            case 3:
                _a.sent();
                return [2 /*return*/, res.sendStatus(200)];
            case 4:
                error_1 = _a.sent();
                return [2 /*return*/, res.sendStatus(400)];
            case 5: return [3 /*break*/, 9];
            case 6:
                _a.trys.push([6, 8, , 9]);
                console.log("I am updating");
                return [4 /*yield*/, CrossReference.update({ Retailer_ID: res.locals.strongParams['Retailer'] }, {
                        $push: {
                            ID: [{
                                    Medicine_ID: res.locals.strongParams['Medicine_ID'],
                                    count: res.locals.strongParams['count'],
                                    Medicine_name: res.locals.strongParams['Medicine_name']
                                }]
                        }
                    })];
            case 7:
                _a.sent();
                console.log("I am after updating");
                return [2 /*return*/, res.sendStatus(200)];
            case 8:
                err_3 = _a.sent();
                return [2 /*return*/, res.sendStatus(400)];
            case 9: return [2 /*return*/];
        }
    });
}); });
// --------------- Update Count -----------------------------
app.post('/updateMedicine', [cookie_middleware_1.customMiddleware({ requireCookie: true })], function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, Retailer_ID, Medicine_ID, value;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, Retailer_ID = _a.Retailer_ID, Medicine_ID = _a.Medicine_ID, value = _a.value;
                console.log("The Retailer ID is:" + Retailer_ID);
                console.log("The Medicine_ID is: " + Medicine_ID);
                if (!(value === 1)) return [3 /*break*/, 2];
                return [4 /*yield*/, CrossReference.updateOne({
                        Retailer_ID: Retailer_ID,
                        "ID.Medicine_ID": Medicine_ID
                    }, { $inc: { "ID.$.count": 1 } })];
            case 1:
                _b.sent();
                return [3 /*break*/, 4];
            case 2: return [4 /*yield*/, CrossReference.updateOne({
                    Retailer_ID: Retailer_ID,
                    "ID.Medicine_ID": Medicine_ID
                }, { $inc: { "ID.$.count": -1 } })];
            case 3:
                _b.sent();
                _b.label = 4;
            case 4:
                console.log("I have successfully incremented");
                return [2 /*return*/, res.sendStatus(200)];
        }
    });
}); });
// ----------- route to check if session exists -----------------
app.get('/session-exists', [cookie_middleware_1.customMiddleware({ requireCookie: true })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (response.locals.userCookie) {
            return [2 /*return*/, response.sendStatus(200)];
        }
        return [2 /*return*/];
    });
}); });
// ------------------ Deleting a Medicine ----------------------
app.post('/delete', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, Retailer_ID, Medicine_ID, err_4;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                console.log("I am in the delete request");
                _a = req.body, Retailer_ID = _a.Retailer_ID, Medicine_ID = _a.Medicine_ID;
                console.log("The retailer ID is: " + Retailer_ID);
                console.log("The medicine ID is: + " + Medicine_ID);
                _b.label = 1;
            case 1:
                _b.trys.push([1, 3, , 4]);
                return [4 /*yield*/, CrossReference.updateOne({ Retailer_ID: Retailer_ID }, { $pull: { ID: { Medicine_ID: Medicine_ID } } })];
            case 2:
                _b.sent();
                return [2 /*return*/, res.sendStatus(200)];
            case 3:
                err_4 = _b.sent();
                console.log(err_4);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
// -------------- Code to get company name from retailer ID ----------------
app.get('/Company/:retailerId', [cookie_middleware_1.customMiddleware({ requireCookie: true })], function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var name, Company, obj, err_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("I am in this end point");
                name = req.params.retailerId;
                console.log(name);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, Retailer.findOne({ Retailer_ID: { $eq: name } }, { Company_name: 1, _id: 0 })];
            case 2:
                Company = _a.sent();
                obj = { Company_name: Company.Company_name };
                console.log("The name of the company is: " + Company.Company_name);
                return [2 /*return*/, res.send(obj).status(200)];
            case 3:
                err_5 = _a.sent();
                console.log("Could not fetch company: " + err_5);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
//  ------------ Code to check if Medicine Exists ------------------------
app.get("/Medicine/:id/:retailerId", [cookie_middleware_1.customMiddleware({ requireCookie: true })], function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, retailerId, response, obj, err_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("I am checking if medicine exists");
                id = req.params.id;
                retailerId = req.params.retailerId;
                console.log("ID is: " + id);
                console.log("Retailer id is: " + retailerId);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, CrossReference.findOne({ $and: [{ Retailer_ID: { $eq: retailerId } }, { "ID": { $elemMatch: { Medicine_ID: id } } }] })];
            case 2:
                response = _a.sent();
                console.log(response);
                if (response) {
                    return [2 /*return*/, res.sendStatus(200)];
                }
                else {
                    obj = { msge: "Not Found" };
                    return [2 /*return*/, res.send(obj).status(200)];
                }
                return [3 /*break*/, 4];
            case 3:
                err_6 = _a.sent();
                console.log("Error in fetching medicine from the databse: " + err_6);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.listen(5000, function () { console.log("I am listening to the port 5000"); });
// Pending work is to code 6 unit test cases to check functioning of each middlewares. Together 12 test cases.
// Now I am able insert data, however I want. I should be able to train the graph ql
// Keep in mind of N+1 Problem
