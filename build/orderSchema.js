"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = __importStar(require("mongoose"));
var orderSchema = new mongoose.Schema({
    //At most medicine ID has 7 digits
    Medicine_ID: {
        type: Number,
        validate: {
            validator: function (medid) {
                return medid > 0 && medid < 9999999;
            }
        },
        required: true
    },
    // Count cannot be less than 0
    count: {
        type: Number,
        validate: {
            validator: function (count) {
                return count > 0;
            }
        },
        required: true
    },
    // Notes length cannot be greater than 500 characters.
    Notes: {
        type: String,
        validate: {
            validator: function (notes) {
                return notes.length < 500;
            }
        }
    }
});
exports.Order = mongoose.model('Order', orderSchema);
