"use strict";
/*
I used websocket to enable communication between retailers and available manufacturers
Retailer sends a message to request the stock of medicines he/she is running out of
Manufacturer checks his production and if available he/she sends the details such as number of days in which he can send the stock,
estimated price etc.
Now the details are available for the retailer to verify

 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Websocket = require("ws");
// These are the models that I used to insert communications between client and server
var MedicineRequest = require('./Models/MedicineRequestSchema').MedicineRequest;
var MedicineResponse = require('./Models/MedicineResponseSchema').MedicineResponse;
var RegularMessages = require('./Models/RegularMessagesSchema').RegularMessages;
var mongoose = require("mongoose");
var port = 5500;
// This is to instantiate a websocket
var WebsocketServer = new Websocket.Server({ port: port });
var myMap = new Map(); // map to store all requests
// Connect to the databse
var db_connect = mongoose.connect("mongodb://localhost:27017/testdb", { useNewUrlParser: true });
// Now we have to create a route
WebsocketServer.on("connection", function (WebsocketClient, req) { return __awaiter(void 0, void 0, void 0, function () {
    var actorId;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                // I am sending a different url from both retailer and manufacturer to differentiate
                // then extracting the word (retailer or manufacturer)
                WebsocketClient.send("You are successfully connected to the server");
                actorId = (req.url).replace('/', "");
                console.log("The actor id is: " + actorId);
                console.log("The length of the actor id is: " + actorId.length);
                if (!(actorId.length === 11)) return [3 /*break*/, 2];
                console.log("You are a client");
                return [4 /*yield*/, RegularMessages.create({ Source: "Server", Destination: "Retailer", Message: "You are successfully connected to the server" })];
            case 1:
                _a.sent();
                console.log("A new retailer is connected");
                myMap.set(actorId, WebsocketClient);
                return [3 /*break*/, 4];
            case 2: return [4 /*yield*/, RegularMessages.create({ Source: "Server", Destination: "Manufacturer", Message: "You are successfully connected to the server" })];
            case 3:
                _a.sent();
                console.log("A new manufacturer is connected");
                console.log("The actor id in the manufacturer chunk is: " + actorId);
                myMap.set(actorId, WebsocketClient); // Add to list of manufacturers
                _a.label = 4;
            case 4:
                // The real part where, receiving message from client comes on:
                WebsocketClient.on('message', function (message) { return __awaiter(void 0, void 0, void 0, function () {
                    var obj, iterator, i, key, socket, obj1, valid_response, socket;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                console.log('Received message from client: ', message);
                                obj = JSON.parse(message);
                                if (!(obj.id.length === 11)) return [3 /*break*/, 6];
                                console.log("The index of the array is: " + obj.id);
                                // Storing the request in the database and after that sending it to the Manufacturer
                                return [4 /*yield*/, MedicineRequest.create({
                                        MedicineCount: obj.MedicineCount,
                                        MedicineID: obj.MedicineID,
                                        RetailerID: obj.id,
                                        Company_name: obj.Company_name,
                                        Notes: obj.Notes,
                                        by: "Retailer"
                                    })];
                            case 1:
                                // Storing the request in the database and after that sending it to the Manufacturer
                                _a.sent();
                                iterator = myMap.keys();
                                i = 0;
                                _a.label = 2;
                            case 2:
                                if (!(i < myMap.size)) return [3 /*break*/, 5];
                                key = iterator.next().value;
                                console.log(key.length);
                                if (!(key.length === 15)) return [3 /*break*/, 4];
                                console.log("I have detected a Manufacturer, I will send him the message");
                                socket = myMap.get(key);
                                if (!(socket.readyState === Websocket.OPEN)) return [3 /*break*/, 4];
                                socket.send(message);
                                // Code to delete the response for particular Medicine ID from the server
                                return [4 /*yield*/, MedicineResponse.deleteOne({ MedicineID: { $eq: obj.MedicineID } })];
                            case 3:
                                // Code to delete the response for particular Medicine ID from the server
                                _a.sent();
                                _a.label = 4;
                            case 4:
                                i++;
                                return [3 /*break*/, 2];
                            case 5: return [3 /*break*/, 10];
                            case 6:
                                obj1 = JSON.parse(message);
                                return [4 /*yield*/, MedicineRequest.findOne({ MedicineID: { $eq: obj1.Medicine_ID }, RetailerID: { $eq: obj1.Retailer_ID } })];
                            case 7:
                                valid_response = _a.sent();
                                if (!valid_response) return [3 /*break*/, 10];
                                console.log("The new retailer id is: " + obj1.Retailer_ID);
                                // Storing the response in the database
                                return [4 /*yield*/, MedicineResponse.create({
                                        Duration_in_days: obj1.Duration,
                                        Cost: obj1.Cost,
                                        Note: obj1.Notes,
                                        MedicineID: obj1.Medicine_ID,
                                        RetailerID: obj1.Retailer_ID,
                                        ManufacturerID: obj1.id,
                                        by: "Manufacturer"
                                    })];
                            case 8:
                                // Storing the response in the database
                                _a.sent();
                                socket = myMap.get(obj1.Retailer_ID);
                                if (!(socket.readyState === Websocket.OPEN)) return [3 /*break*/, 10];
                                socket.send(message);
                                // Code to delete the request for particular Medicine from the database.
                                // This means that request is already served
                                return [4 /*yield*/, MedicineRequest.deleteOne({ MedicineID: { $eq: obj1.Medicine_ID } })];
                            case 9:
                                // Code to delete the request for particular Medicine from the database.
                                // This means that request is already served
                                _a.sent();
                                _a.label = 10;
                            case 10: return [2 /*return*/];
                        }
                    });
                }); });
                console.log('Broadcasted message to all connected clients!');
                return [2 /*return*/];
        }
    });
}); });
console.log('Websocket server is up and ready for connections on port', { port: port });
// logic to send the response to only concerned retailer
// Without retailer id, we have to design a new logic
// Instead of that, I will map, unique id to the concerned retailer.
// Every time, that retailer sends the message he, has to remember the unique id.
// Now at 4:21 PM, If we can create proper front end for Manufacturer, we can see the message sent by the client
