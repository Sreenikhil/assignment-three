"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var TextField_1 = __importDefault(require("@material-ui/core/TextField"));
var styles_1 = require("@material-ui/core/styles");
var Button_1 = __importDefault(require("@material-ui/core/Button"));
var Navigation_1 = require("./Navigation");
var Footer_1 = require("./Footer");
var react_router_dom_1 = require("react-router-dom");
var useStyles = styles_1.makeStyles(function (theme) { return ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 150,
        border: 5,
        borderColor: 'black',
        padding: '5rem',
        backgroundColor: 'gray',
        marginLeft: 250,
        marginRight: 250
    },
    field: {
        margin: theme.spacing(1),
        width: '30ch',
    },
    userButton: {
        marginTop: '1rem',
        marginRight: '3rem'
    },
    loginButton: {
        marginTop: '1rem'
    },
    inroot: {
        display: 'flex',
        flexDirection: 'row',
        margin: theme.spacing(0, 0, 1, 0),
    },
}); });
exports.Login = function () {
    var classes = useStyles();
    var history = react_router_dom_1.useHistory();
    function onSignup() {
        history.push('/Signup');
    }
    return (<div>
          <Navigation_1.Navigation />
      <div className={classes.root}>
         <TextField_1.default className={classes.field} id="outlined" margin="normal" label="Retailer-ID" variant="outlined" size="medium"/>
         <TextField_1.default className={classes.field} id="outlined" margin="normal" label="Password" type="password" variant="outlined" size="medium"/>

         <div className={classes.inroot}>
            <Button_1.default className={classes.userButton} onClick={onSignup} variant="contained" color="secondary">
               New Member
            </Button_1.default>

            <Button_1.default className={classes.loginButton} variant="contained" color="secondary">
               Login
            </Button_1.default>
         </div>
         </div>
          <Footer_1.Footer />
          </div>);
};
