"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
exports.MedicineInput = new graphql_1.GraphQLInputObjectType({
    name: 'MedicineInput',
    description: 'Input fields for the Medicine model',
    fields: function () { return ({
        Medicine_name: { type: graphql_1.GraphQLString },
        count: { type: graphql_1.GraphQLInt },
        Medicine_ID: { type: graphql_1.GraphQLInt }
    }); },
});
