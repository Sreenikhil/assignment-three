"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var express_graphql_1 = __importDefault(require("express-graphql"));
var mongoose_1 = __importDefault(require("mongoose"));
var GraphQLSchema_1 = __importDefault(require("./GraphQLSchema"));
var cors_1 = __importDefault(require("cors"));
var cookie_middleware_1 = require("../Middlewares/cookie_middleware");
// Instantiate a new Express application (same as we always did)
var app = express_1.default();
// Connect to a local 'test' MongoDB
mongoose_1.default.connect('mongodb://localhost:27017/testdb', { useNewUrlParser: true });
// When clients go to '/graphql', they will be able to access our GraphQL endpoint
// Notice how this looks kind of like a middleware, use() is actually a request handler for the whole application
// Here we are using a request handler for the /graphql route only
var corsOptions = {
    origin: 'http://localhost:3000',
    credentials: true,
};
app.use(cors_1.default(corsOptions));
app.use('/graphql', express_graphql_1.default(function (req, res) { return (({
    schema: GraphQLSchema_1.default,
    context: { req: req, res: res },
    graphiql: true
})); }));
app.use(cookie_middleware_1.customMiddleware(({ requireCookie: true })));
// Have our Express server listen on port 4000
app.listen(4000, function () { return console.log('Server is up!'); });
