"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
// Graphql object that will represent our mongoose model
exports.GraphQlRetailer = new graphql_1.GraphQLObjectType({
    name: 'Retailer',
    description: 'Retailer of the website',
    // Fields represent what can GraphQl return from the User object
    fields: function () { return ({
        // There are no resolvers specified for the firstName or lastName field, GraphQL will attempt to get this
        // information bu performing user.firstName and user.lastName respectively, which will work!
        Company_name: { type: graphql_1.GraphQLString, resolve: undefined },
        Country_code: { type: graphql_1.GraphQLInt },
        Phone_Number: { type: graphql_1.GraphQLInt },
        Address: { type: graphql_1.GraphQLString },
        Country: { type: graphql_1.GraphQLString },
        City: { type: graphql_1.GraphQLString },
        Retailer_ID: { type: graphql_1.GraphQLString },
        Password: { type: graphql_1.GraphQLString },
    }); }
});
