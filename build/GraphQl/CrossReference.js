"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var Medicine_1 = require("./Medicine");
// Graphql object that will represent our mongoose model
exports.GraphQLCrossReference = new graphql_1.GraphQLObjectType({
    name: 'CrossReference',
    description: 'CrossReference of the website',
    // Fields represent what can GraphQl return from the User object
    fields: function () { return ({
        // There are no resolvers specified for the firstName or lastName field, GraphQL will attempt to get this
        // information bu performing user.firstName and user.lastName respectively, which will work!
        Retailer_ID: { type: graphql_1.GraphQLString },
        ID: {
            type: new graphql_1.GraphQLList(Medicine_1.GraphQlMedicine),
            resolve: function (parent, args) {
                return parent.ID;
            }
        }
    }); }
});
