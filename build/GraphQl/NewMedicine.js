"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
// Graphql object that will represent our mongoose model
exports.GraphQlNewMedicine = new graphql_1.GraphQLObjectType({
    name: 'NewMedicine',
    description: 'Medicine of the website',
    // Fields represent what can GraphQl return from the User object
    fields: function () { return ({
        // There are no resolvers specified for the firstName or lastName field, GraphQL will attempt to get this
        // information bu performing user.firstName and user.lastName respectively, which will work!
        _id: { type: graphql_1.GraphQLString },
        Medicine_name: { type: graphql_1.GraphQLString },
        count: { type: graphql_1.GraphQLInt },
        Medicine_ID: { type: graphql_1.GraphQLInt }
    }); }
});
