"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var MedicineRequestSchema = new mongoose.Schema({
    MedicineCount: {
        type: Number,
        required: true // Built-In validation
    },
    MedicineID: {
        type: Number,
        required: true
    },
    RetailerID: {
        type: String,
        required: true
    },
    Company_name: {
        type: String,
        required: true
    },
    Notes: {
        type: String,
        required: true
    },
    by: {
        type: String,
        required: true
    }
});
exports.MedicineRequest = mongoose.model('Request', MedicineRequestSchema);
