const sinon = require('sinon');
const httpmock = require('node-mocks-http');
const assert = require('chai').assert;
const expect = require('chai').expect;
const sample = require('./sample');

describe('mock-test', function(){
    it("sample is called", function(){
        const sample = sinon.spy();
        sample()
        sinon.assert.calledOnce(sample);

    })
});