import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Components/App';
import * as serviceWorker from './serviceWorker';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from '@apollo/react-hooks'
import {HttpLink} from 'apollo-boost'
import {combineReducers, createStore} from "redux";
import {RetailerReducer} from "./Redux/Reducer/RetailerReducer";
import {Provider} from "react-redux";
import {IRetailer} from './Redux/State/IRetailer'


const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql'
});



function saveToLocalStorage(state: any){
try{
    const serializableState = JSON.stringify(state)
    localStorage.setItem('state', serializableState)
}catch(e){
    console.log(e)
}
}


function loadFromLocalStorage(){
    try{
        const serializedState = localStorage.getItem('state')
        if(serializedState === null) return undefined
        return JSON.parse(serializedState)
    }
    catch(e){
        console.log(e)
        return undefined
    }
}


const rootReducer = combineReducers({
    RetailerReducer,
});

const persistedState = loadFromLocalStorage()

const store = createStore(
    rootReducer,
    persistedState
);

store.subscribe(() =>
    saveToLocalStorage(store.getState()))


ReactDOM.render(
    <Provider store={store}>
  <React.StrictMode>
      <ApolloProvider client={client}>
       <App />
    </ApolloProvider>
  </React.StrictMode>
        </Provider>,

  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
