import React, {useEffect, useState} from 'react'
import {HomeNavigation} from "./HomeNavigation";
import {useHistory, useParams} from "react-router-dom";
import gql from "graphql-tag";
import {useQuery} from "@apollo/react-hooks";
import Button from "@material-ui/core/Button";
import {DisplayMedicine} from "./DisplayMedicines";
import {useSelector} from "react-redux";
import {IRetailerReducer} from "../IRetailerReducer";



//Let me define the graphql query



interface MedicineVars {
    Retailer_ID: string;
}

interface Medicine{
    Medicine_ID: number,
    Medicine_name: string
    count: number,
}

interface AllMedicines{
    Retailer_ID: string,
    ID: Medicine[]
}

// Query which satisfies the requirement of:
// (1) At least one (1) Query must be able to provide at least one (1) field of a child object and
// (2) one of which that takes in at least one (1) parameter and also
//  Each query should be able to provide at least three (3) fields worth of information.


const Get_Med = gql `
    query abc($Retailer_ID: String!){
        AllMedicines(Retailer_ID: $Retailer_ID){
            ID{
                Medicine_ID,
                count,
                Medicine_name
            }
        }
    }`;

export const  ViewMedicine : React.FC = ()=>{
    const retailerId : string | undefined = useSelector<IRetailerReducer,string| undefined>(state => state.RetailerReducer.Retailer_ID)
    console.log("I know who you are: "+ retailerId)
    const [Meds, setMeds] = useState([])

    const {loading, error, data} = useQuery(Get_Med, {
        variables: {Retailer_ID: retailerId},
    });



    function debugFunction() {
        console.log("I am clicked, debugger")
    }

   if(loading){
       return(
           <h1> I am loading </h1>
       )
   }

   if(error){
       return(
           <h1> I have encountered an error </h1>
       )
   }


    return(
        <div>
        <HomeNavigation />
            {
            data?.AllMedicines?.ID?.map((result: Medicine) =>(
                <DisplayMedicine Medicine_name={result.Medicine_name}
                                 count = {result.count}
                                 Medicine_ID={result.Medicine_ID}

                  />

            ))}


        </div>

        // The logic is to make query based on the retailer id
        // I will come back with in a short time

    )
}
