import React, {useEffect, useState} from 'react'
import TextField from "@material-ui/core/TextField/TextField";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Axios from 'axios'
import {useMutation, useQuery} from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import {useSelector} from "react-redux";
import {HomeNavigation} from "./HomeNavigation";


const useStyles = makeStyles((theme) =>({
    root:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 150,
        border: 5,
        borderColor: 'black',
        padding: '5rem',
        backgroundColor: 'gray',
        marginLeft: 250,
        marginRight: 250
    },
    field:{
        margin: theme.spacing(1),
        width: '30ch',
    },
    userButton:{
        marginTop: '1rem',
        marginRight: '3rem'
    },
    loginButton:{
        marginTop: '1rem'
    },
    inroot:{
        display: 'flex',
        flexDirection: 'row',
        margin: theme.spacing( 0, 0, 1, 0),
    },
}))

export const ResponseRetailer: React.FC = ()=>{
    const classes = useStyles();
    const [websocket, setwebsocket] = useState<WebSocket>(Object)

    const [duration, setDuration] = useState(0)
    const [cost, setCost] = useState(0)
    const [note, setNote] = useState('')
    const [retailer, setRetailer] = useState('')
    const [Rmessage, setRMessage] = useState<MessageEvent[]>([])
    const [unique, setUnique] = useState('')
    const [medicineId, setMedicineId] = useState(0)


    useEffect(() => {
        const chars = "'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'"
        let result = '';
        for (let i = 15; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        const uniqueId: string = result
        console.log("The unique ID from the Manufacturer side is: "+ uniqueId)
        setUnique(uniqueId)
        const webSocket = new WebSocket('ws://localhost:5500/'+uniqueId);
        (async () => {
            console.log("I am setting a websocket")
            setwebsocket(webSocket)
            webSocket.onmessage = (message: MessageEvent) => {
                console.log('Received message: ', message);
                setRMessage((oldMessages:MessageEvent[]) => [...oldMessages, message])
            };
        })()
        return () => webSocket.close();

    }, [])

   function durationChange(event: React.ChangeEvent<HTMLInputElement>){
        setDuration(parseInt(event.target.value))
    }

    function costChange(event: React.ChangeEvent<HTMLInputElement>){
     setCost(parseInt(event.target.value))
    }

    function noteChange(event: React.ChangeEvent<HTMLInputElement>){
     setNote(event.target.value)
    }

    function retailerChange(event: React.ChangeEvent<HTMLInputElement>){
        setRetailer(event.target.value)
    }

    function medicineIdChange(event: React.ChangeEvent<HTMLInputElement>){
        setMedicineId(parseInt(event.target.value))
    }

    function submitRequest(){
        console.log("Response details")
        console.log("Duration is: "+ duration)
        console.log("Cost is: "+ cost)
        console.log("Note is: "+ note)
        console.log("Retailer is: "+ retailer)
        console.log("ID is: "+ unique)
        const resObj = {
            Duration: duration,
            Cost: cost,
            Notes: note,
            Retailer_ID: retailer,
            id: unique,
            Medicine_ID: medicineId
        }
        websocket.send(JSON.stringify(resObj))
        alert("Data is sent to the retailer")
    }


    return(
        <div>
           <HomeNavigation/>
            <div className={classes.root}>

                <TextField className={classes.field} id="outlined" margin="normal" label="MedicineID"
                           variant="outlined" size="medium"
                           onChange={medicineIdChange}/>


                <TextField className={classes.field} id="outlined" margin="normal" label="Duration in days"
                           variant="outlined" size="medium"
                           onChange={durationChange}/>

                <TextField className={classes.field} id="outlined" margin="normal" label="cost"
                           variant="outlined" size="medium"
                           onChange={costChange}
                />

                <TextField className={classes.field} id="outlined" margin="normal" label="Note"
                           variant="outlined" size="medium"
                           onChange={noteChange}
                />


                <TextField className={classes.field} id="outlined" margin="normal" label="Retailer ID"
                           variant="outlined" size="medium"
                           onChange={retailerChange}
                />

                <Button className={classes.userButton} onClick={submitRequest} variant="contained" color="secondary">
                    Send Response
                </Button>
            </div>

            <h1> Responses from the server are:  </h1>

            {
                Rmessage?.map((msg: MessageEvent) => {
                    let Obj = JSON.parse(JSON.stringify(msg.data))
                    const k = Obj.split(',')
                    return (
                        <p id="server-msg">Messages from the Retailer: {Obj} </p>

                    )
                })
            }
        </div>
    )
}

