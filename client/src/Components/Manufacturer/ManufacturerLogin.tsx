import React, {useState} from "react";
import TextField from '@material-ui/core/TextField';
import {makeStyles} from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";
import Axios from 'axios'
import {useDispatch, useSelector} from "react-redux";
import {ResponseRetailer} from "./ResponseRetailer";
import {Navigation} from '../Navigation'



const useStyles = makeStyles((theme) =>({
    root:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 150,
        border: 5,
        borderColor: 'black',
        padding: '5rem',
        backgroundColor: 'gray',
        marginLeft: 250,
        marginRight: 250
    },
    field:{
        margin: theme.spacing(1),
        width: '30ch',
    },
    userButton:{
        marginTop: '1rem',
        marginRight: '3rem'
    },
    loginButton:{
        marginTop: '1rem'
    },
    inroot:{
        display: 'flex',
        flexDirection: 'row',
        margin: theme.spacing( 0, 0, 1, 0),
    },
}))

export const ManufacturerLogin: React.FC = ()=> {
    const classes = useStyles();
    const history = useHistory();
    const [Id, setId] = useState('')
    const [password, setPassword] = useState('')


    function retailerChange(event: React.ChangeEvent<HTMLInputElement>){
        setId(event.target.value)
    }

    function passwordChange(event:React.ChangeEvent<HTMLInputElement>){
        setPassword(event.target.value)
    }

    async function onLogin() {
        if(Id === "Admin" && password === "Password"){
            history.push('/ResponseRetailer')
        }
    }

    return(
        <div>
        <Navigation/>
        <div className={classes.root}>
        <TextField  className={classes.field} id="outlined"  margin="normal" label="Retailer-ID"
        variant="outlined" size = "medium"
        onChange = {retailerChange}/>

        <TextField  className={classes.field}  id="outlined"  margin="normal"  label="Password"
         type = "password"  variant="outlined" size = "medium"
         onChange = {passwordChange}
         />

         <div className = {classes.inroot}>
         <Button className = {classes.loginButton} onClick = {onLogin} variant="contained" color="secondary">
         Login
         </Button>
         </div>
         </div>

         </div>
     )
   }