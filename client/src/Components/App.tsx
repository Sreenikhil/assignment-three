import React from 'react'
import {
    BrowserRouter as Router,
    Route,
} from "react-router-dom";
import {Login} from './Login'
import {Signup} from './Signup'
import {Navigation} from "./Navigation";
import {About} from "./About";
import {Home} from "./Home"
import {AddMedicine} from "./AddMedicine";
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import {ViewMedicine} from "./ViewMedicines";
import {NotifyManufacturer} from "./NotifyManufacturer";
import {ResponseRetailer} from "./Manufacturer/ResponseRetailer";
import {MainRoute} from "./MainRoute";
import {ManufacturerLogin} from './Manufacturer/ManufacturerLogin'

// Here, I am satisfying the requirement of:
// 2. Your code must include at least three (3) separate pages using React router.
//  Your code must consist of at least five (5) separate React components.
function App(){
    return(
       <div>
           <Router>
               <switch>
                   <Route exact path = "/"  component = {MainRoute} />
                   <Route exact path = "/Client"  component = {Login} />
                   <Route exact path = "/Manufacturer"  component = {ManufacturerLogin} />
                   <Route exact path = "/Signup"  component = {Signup} />
                   <Route exact path = "/About"  component = {About} />
                   <Route exact path = "/Home"  component = {Home} />
                   <Route exact path = "/AddMedicine"  component = {AddMedicine} />
                   <Route exact path = "/ViewMedicine"  component = {ViewMedicine} />
                   <Route exact path = "/Notify"  component = {NotifyManufacturer} />
                   <Route exact path = "/ResponseRetailer"  component = {ResponseRetailer} />


               </switch>
           </Router>
           </div>
    )
}


export default App


/*
New schema would have retailer id and an array of medicine ID's
Our job is to make a post request to store Retailer ID and Medicine id as well
 */