import React, {useState} from 'react'
import { useHistory , useParams, RouteComponentProps} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { spacing } from '@material-ui/system';
import Axios from 'axios'
import {useDispatch, useSelector} from "react-redux";
import {DestroyAction} from "../Redux/Action/RetailerAction";
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Drawer from '@material-ui/core/Drawer';


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1,
    },
    deleteJob:{
        marginRight: "35rem"
    },
    sort:{
        marginRight: "40rem"
    },
    profile:{
        marginRight: "0.5rem"
    },
    sidebar: {
        marginTop: "3.5rem",
        width: "15rem",
        marginLeft: "2rem"
    }

}));




export const HomeNavigation: React.FC = () => {

    const {retailerId} = useParams()
    const history = useHistory();
    const classes = useStyles();
    const dispatch = useDispatch();
    const [state, setState] = useState(false)

async function redirectfunction() {
    // Go to Login
    const response = await Axios.get('/logout')
    const {msge, msg} = response.data;
    if(response.status === 200 && msge === "You did a great logout"){
        alert(msge)
    }
     if(response.status === 200 && msg === "Invalid request"){
        alert("No point in logging out for guest user!!")
    }
     dispatch(DestroyAction())
    history.push('/')

}
function addMedicineClick() {
    history.push('/AddMedicine')
}

function viewMedicineClick() {
    history.push('/ViewMedicine')
}

function deleteMedicineClick() {
     history.push('/DeleteMedicine')
}

function notifyMedicineClick(){
        history.push('/Notify')
}

function viewResponsesClick(){
        history.push('/ViewResponses')
}



const toggleDrawer = (open: boolean) => (
        event: React.KeyboardEvent | React.MouseEvent,
    ) => {
        if (
            event.type === 'keydown' &&
            ((event as React.KeyboardEvent).key === 'Tab' ||
                (event as React.KeyboardEvent).key === 'Shift')
        ) {
            return;
        }
        setState(open)
    };



const list = ()=>(
        <div className = {classes.sidebar}>
            <List>
                <ListItem button >
                <Button onClick = {addMedicineClick}> Add Medicine </Button>
                </ListItem>

                <ListItem button >
                    <Button onClick = {viewMedicineClick}> View Medicine </Button>
                </ListItem>

                <ListItem button >
                    <Button onClick = {notifyMedicineClick}> Notify Manufacturer</Button>
                </ListItem>

                <ListItem button >
                    <Button onClick = {viewResponsesClick}> View Responses </Button>
                </ListItem>

            </List>
        </div>
    )

return (
    <div className={classes.root}>
        <AppBar position="static">
            <Toolbar>
                <IconButton onClick={toggleDrawer( true)} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    Menu
                </Typography>

                <Button color="inherit" onClick={redirectfunction}>Logout</Button>
            </Toolbar>
        </AppBar>


        <Drawer  open = {state} onClose={toggleDrawer(false)}>
            {list()}
        </Drawer>


    </div>
);
}




// Design should be neat
/*
When a perfectly valid user login in, he is given 4 options, either to add, delete, update, view,
Logic lies in how to map retailer and the medicines he/she adds.
Get the retailer ID from the cookie. and what ever tasks he/she does map that to that particular user
Maintain a separate collection, which maps, retailer id with all medicine id's he/she has.
*/

// -------------------------------------------------------------------------------------------

/*
While adding medicine, we have to do 2 things
make sure that medicine is successfully added to the database
Also connect medicine id and the retailer adding the medicine in a separate collection
The humongous task is to write a syntax error free graph ql query. At any cost, I will write it
*/

// --------------------------------------------------------------------------------------------

/*
Note: As soon as user logs in successfully, he/she comes here
*/