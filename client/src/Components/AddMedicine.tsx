import React, {useState} from 'react'
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";

import {Navigation} from "./Navigation";
import {Footer} from './Footer'
import {useHistory, useParams} from "react-router-dom";
import Axios from 'axios'
import {HomeNavigation} from "./HomeNavigation";
import {useMutation, useQuery} from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import {useSelector} from "react-redux";
import {IRetailerReducer} from "../IRetailerReducer";






const useStyles = makeStyles((theme) =>({
    root:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 150,
        border: 5,
        borderColor: 'black',
        padding: '5rem',
        backgroundColor: 'gray',
        marginLeft: 250,
        marginRight: 250
    },
    field:{
        margin: theme.spacing(1),
        width: '30ch',
    },
    userButton:{
        marginTop: '1rem',
        marginRight: '3rem'
    },
    loginButton:{
        marginTop: '1rem'
    },
    inroot:{
        display: 'flex',
        flexDirection: 'row',
        margin: theme.spacing( 0, 0, 1, 0),
    },
}))


// Query to add Medicine
// This satisfies the requirement of:
// The GraphQL server must have at at least one (1) Mutator that creates data with at least two (2)
// fields in MongoDB

const Add_Medicine=  gql `
    mutation AddMed($Medicine_name: String!, $count: Int!, $Medicine_ID: Int!){
        createMedicine(input:{Medicine_ID: $Medicine_ID, count: $count, Medicine_name: $Medicine_name}){
            Medicine_ID
        }
    }
`;

// Query to get Medicine.
// This satisfies the requirement of:
// Each query should be able to provide at least three (3) fields worth of information.
const Get_Medicine = gql `
    query GetMed($Medicine_ID: Int!){
    Medicine(Medicine_ID: $Medicine_ID){
       Medicine_name,
        count,
        Medicine_ID
    }
    }

`;





interface MedicineVars{
  Medicine_ID: number;
}

interface Medicine{
    Medicine_ID: number,
    count: number,
    Medicine_name: string
}

interface MedicineData{
    Medicine: Medicine
}

export const AddMedicine: React.FC = () =>{
// Here AddMed is newly created

    const classes = useStyles();
    const retailerId : string | undefined = useSelector<IRetailerReducer,string| undefined>(state => state.RetailerReducer.Retailer_ID)
    console.log(retailerId)
    const [medid, setMedid] = useState(0)
    const [count, setCount] = useState(0)
    const [medname, setMedname] = useState('')
    const [createMedicine] = useMutation(Add_Medicine);


    console.log("I am here")
    const { loading, error, data } = useQuery<MedicineData,MedicineVars >(Get_Medicine, {
        variables: { Medicine_ID: medid },
    });



    function nameChange(event:  React.ChangeEvent<HTMLInputElement>){
        setMedname(event.target.value)
    }

    function idChange(event:  React.ChangeEvent<HTMLInputElement>) {
        setMedid(parseInt(event.target.value))
    }

    function countChange(event: React.ChangeEvent<HTMLInputElement>){
        setCount(parseInt(event.target.value))
    }

    async function submitMedicine(){
        console.log("I will use apollo server to mutate the medicine in to the database")
        console.log(medid)
        console.log(count)
        console.log(medname)
        // Allowing user to perform action only if session exists, else no
        const response = await Axios.get('/session-exists')
        const {msg} = response.data
        if(response.status === 200 && msg === "Invalid request"){
            alert("I am sorry, you are a guest user here!! You cannot make any changes")
        }
        else {
            const medicineid = data?.Medicine?.Medicine_ID
            if (!medicineid) {
                alert("I am doing a great job")
                // Graphql Request to add medicine to medicines table
                await createMedicine({variables: {Medicine_ID: medid, count: count, Medicine_name: medname}});
                // Adding medicine to cross reference collection
                // Before,I was sending objects which is not a good practise, so I will not send objects any more

                const crossRefObj = {
                    Retailer: retailerId,
                    Medicine_ID: medid,
                    count: count,
                    Medicine_name: medname
                }
                const response = await Axios.post("/addcross", crossRefObj)

                if (response.status === 200) {
                    alert("I am successful in adding the medicine")
                }
                console.log("The response is: " + data)
            } else {
                alert("I am sorry!! Medicine ID akready exists")
            }
        }

    }




    return(

        <div>
        <HomeNavigation />
        <div className={classes.root}>
            <TextField  className={classes.field} id="outlined"  margin="normal" label="Medicine-ID"
                        variant="outlined" size = "medium"
                        onChange = {idChange}/>

            <TextField  className={classes.field}  id="outlined"  margin="normal"  label="count"
                         variant="outlined" size = "medium"
                        onChange = {countChange}
            />

            <TextField  className={classes.field}  id="outlined"  margin="normal"  label="Medicine-Name"
                        variant="outlined" size = "medium"
                        onChange = {nameChange}
            />

            <Button className = {classes.userButton} onClick = {submitMedicine} variant="contained" color="secondary">
                  Add Medicine
            </Button>


        </div>
            <Footer/>
        </div>

    )

}