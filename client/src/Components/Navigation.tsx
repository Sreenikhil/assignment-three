import React from 'react'
import Button from '@material-ui/core/Button';
import {makeStyles} from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) =>({
    root:{
        display: 'flex',
        flexDirection: 'row',
        border: 10,
        borderColor: 'black',
        padding: '0.3rem',
        backgroundColor: 'gray',
        justifyContent: 'space-around'
    },
    button:{
        width: '9rem',
        height: '3rem',
        marginTop: '1rem',
    },
    text:{
        marginLeft: '28rem',
    }
}))


export const Navigation: React.FC = ()=>{
    const classes = useStyles();
    const history = useHistory();
    function GoAbout(){
        history.push('/About')
    }
    return(
     <div className={classes.root}>
         <h1 className={classes.text}> Pharma Solutions </h1>
         <Button className={classes.button} onClick = {GoAbout} variant="contained" color="secondary"> About </Button>
         </div>
    )
}