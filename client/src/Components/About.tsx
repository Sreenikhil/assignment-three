import React from 'react'
import {makeStyles} from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";


const useStyles = makeStyles((theme) =>({
    root:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 150,
        border: 5,
        borderColor: 'black',
        backgroundColor: 'gray',
        marginLeft: 300,
        marginRight: 250,
        width: "50rem",
        height: "20rem"
    },

}))

export const About: React.FC = ()=>{
    const classes = useStyles();
    const history = useHistory();
    function GoLogin() {
        history.push('/')
    }
    return (
        <div style = {{textAlign:'center'}} className={classes.root}>
            <p >  Pharma Pharma Solutions is an application
                to solve the global problem of lack of medicines at
                right time to the people It
            It brings retailers and manufacturers in the pharma industry under one roof <br />
                Retailers keeps an eye on the stock and request manufacturers when they need <br />
                Manufacturers checks their production and based on their availability they send response to retailer <br />
                Retailers receives responses from all the manufacturers collaborating with pharma solutions and order from the manufacturer of their choice
                    </p>

            <Button  variant="contained" color="secondary" onClick = {GoLogin}> Return to Login </Button>
            </div>
    )
}