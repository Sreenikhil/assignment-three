import React from 'react'
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";


const useStyles = makeStyles((theme) =>({
    root:{
        padding: '0.3rem',
        backgroundColor: 'gray',
        marginTop: '8rem'

    },
    text:{
        marginLeft: '40rem',
    }
}))

export const Footer: React.FC = ()=>{
    const classes = useStyles();
    return(
        <div className={classes.root}>
            <p className={classes.text}> &copy; 2020 Pharma Solutions </p>
        </div>
    )
}