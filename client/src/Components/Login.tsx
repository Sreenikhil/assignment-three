import React, {useState} from "react";
import TextField from '@material-ui/core/TextField';
import {makeStyles} from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import {Navigation} from "./Navigation";
import {Footer} from './Footer'
import { useHistory } from "react-router-dom";
import Axios from 'axios'
import {useDispatch, useSelector} from "react-redux";
import {IRetailerReducer} from "../IRetailerReducer";
import {RetailerAction} from "../Redux/Action/RetailerAction";


//  At least one (1) global Redux state must be integrated into your front-end. I have stored, Retailer ID

const useStyles = makeStyles((theme) =>({
   root:{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 150,
      border: 5,
      borderColor: 'black',
      padding: '5rem',
      backgroundColor: 'gray',
      marginLeft: 250,
      marginRight: 250
   },
   field:{
      margin: theme.spacing(1),
      width: '30ch',
   },
   userButton:{
      marginTop: '1rem',
      marginRight: '3rem'
   },
   loginButton:{
      marginTop: '1rem'
   },
   inroot:{
      display: 'flex',
      flexDirection: 'row',
      margin: theme.spacing( 0, 0, 1, 0),
   },
}))

export const Login: React.FC = ()=> {
   const classes = useStyles();
   const history = useHistory();
   const dispatch = useDispatch();
   const [retailerId, setretailerId] = useState('')
   const [password, setPassword] = useState('')
   //const Retailer_ID: string | undefined = useSelector<IRetailerReducer,string| undefined>(state => state.RetailerReducer.Retailer_ID)
   function onSignup(){
      history.push('/Signup')
   }

   function retailerChange(event: React.ChangeEvent<HTMLInputElement>){
      setretailerId(event.target.value)
   }

   function passwordChange(event:React.ChangeEvent<HTMLInputElement>){
      setPassword(event.target.value)
   }

   async function onLogin() {
      console.log("The password is: "+ password)
      console.log("The type of password is: "+ typeof (password))
      const loginObj = {
         Retailer_ID: retailerId,
         Password: password
      }
      const response = await Axios.post('/session', loginObj)
      const {msg, msgs} = response.data

      if(response.status === 200 && !msg){
         alert("Successful Login");
         dispatch(RetailerAction(retailerId))
         history.push('/Home')
      }
      if(response.status === 200 && msg === "Invalid request"){
         alert("Invalid Username/Password")
      }
   }

   return(
       <div>
          <Navigation />
      <div className={classes.root}>
         <TextField  className={classes.field} id="outlined"  margin="normal" label="Retailer-ID"
                     variant="outlined" size = "medium"
                     onChange = {retailerChange}/>

         <TextField  className={classes.field}  id="outlined"  margin="normal"  label="Password"
                     type = "password"  variant="outlined" size = "medium"
                     onChange = {passwordChange}
         />

         <div className = {classes.inroot}>
            <Button className = {classes.userButton} onClick = {onSignup} variant="contained" color="secondary">
               New Member
            </Button>

            <Button className = {classes.loginButton} onClick = {onLogin} variant="contained" color="secondary">
               Login
            </Button>
         </div>
         </div>
          <Footer />
          </div>
   )
}