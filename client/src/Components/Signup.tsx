import React,  { useState } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {makeStyles} from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import Axios from 'axios'


const useStyles = makeStyles((theme) =>({
    root:{
        display: 'flex',
        flexDirection: 'column',
        border: 10,
        alignItems: 'center',
        borderColor: 'black',
        padding: '3rem',
        backgroundColor: 'gray',
        marginLeft: '10rem',
        marginRight: '10rem',
        marginTop: '5rem'
    },
    field:{
        margin: theme.spacing(1),
        width: '30ch',
    },
    inroot:{
        display: 'flex',
        flexDirection: 'row',
        margin: theme.spacing( 0, 0, 1, 0),
    },
    codefield:{
        margin: theme.spacing( 1, 1, 0, 17),
        width: '10ch'
    },
    phonefield:{
        margin: theme.spacing( 1, 2, 0),
        width: '30ch',
    },
    // (top, right, bottom, left) Clockwise
    country:{
        margin: theme.spacing( 1, 1, 0, 43),
        width: '30ch'
    },
    city:{
        margin: theme.spacing( 1, 3, 0),
        width: '30ch',
    },
    userButton:{
        marginTop: '1rem',
        marginRight: '3rem'
    },
    signUpButton:{
        marginTop: '1rem'
    }

}))





// I am fed up doing this. I will try for middleware

export const Signup: React.FC =  ()=>{

    const history = useHistory();
    const classes = useStyles();
    const [companyName, setCompanyName] = useState('');
    const [countryCode, setCountryCode ] = useState(0);
    const [phoneNumber, setPhoneNumber] = useState(0);
    const [address, setAddress ] = useState('');
    const [country, setCountry] = useState('');
    const [state, setState] = useState('');
    const [retailerID, setRetailerID] = useState('')
    const [password, setPassword ] = useState('');


    function Exists(){
        history.push('/')
    }

     async function doSignUp(){
        console.log("The information that I got is: ")
        console.log("Company name is: "+ companyName)
        console.log("Country Code is: "+ countryCode)
        console.log("Phone number is: "+ phoneNumber)
        console.log("Address is: "+ address)
        console.log("Country is: "+ country)
        console.log("State is: "+ state)
        console.log("Retailer ID is: "+ retailerID)
        console.log("password is: "+ password)
         const signUpObj = {
             Company_name: companyName,
             Country_code: countryCode,
             Phone_Number: phoneNumber,
             Address: address,
             Country: country,
             State: state,
             Retailer_ID: retailerID,
             Password:password
         }
         console.log("Check here is: "+ signUpObj.Company_name)
         const response =  await Axios.post('/retailer-reg', signUpObj)
         const {msg} = response.data
         if(response.status === 200 && !msg){
             console.log("Retailer Registration is successful")
             alert("Registration Successful")
         }
          if(response.status === 200 && msg === "Invalid request") {
              console.log("Retailer registration is unsuccessful")
              alert("Registration Failed")
          }

    }

    function companyChange(event: React.ChangeEvent<HTMLInputElement>){
        setCompanyName(event.target.value)
    }

    function countryCodeChange(event: React.ChangeEvent<HTMLInputElement>){
        setCountryCode(parseInt(event.target.value))
    }

    function phoneNumberChange(event: React.ChangeEvent<HTMLInputElement>) {
        setPhoneNumber(parseInt(event.target.value))
    }

    function addressChange(event: React.ChangeEvent<HTMLInputElement>){
        setAddress(event.target.value)
    }

    function countryChange(event: React.ChangeEvent<HTMLInputElement>){
        setCountry(event.target.value)
    }

    function stateChange(event:  React.ChangeEvent<HTMLInputElement>){
           setState(event.target.value)
    }

    function retailerIDChange(event:  React.ChangeEvent<HTMLInputElement>){
          setRetailerID(event.target.value)
    }

    function passwordChange(event:  React.ChangeEvent<HTMLInputElement>){
         setPassword(event.target.value)
    }

    return(

        <div  className={classes.root}>
        <TextField  className={classes.field} id="outlined"  margin="normal" label="Company Name"   variant="outlined" size = "medium" onChange = {companyChange} />

        <div className={classes.inroot}>
        <TextField  className={classes.codefield} id="outlined"  margin="normal" label="Code"   variant="outlined" size = "medium" onChange = {countryCodeChange} />
        <TextField  className={classes.phonefield} id="outlined"  margin="normal" label="Phone Number"   variant="outlined" size = "medium" onChange = {phoneNumberChange} />
        </div>

        <TextField  className={classes.field} id="outlined"  margin="normal" label="Address"   variant="outlined" size = "medium"  onChange = {addressChange}/>

        <div className={classes.inroot}>
        <TextField  className={classes.country} id="outlined"  margin="normal" label="Country"   variant="outlined" size = "medium"   onChange = {countryChange}/>
        <TextField  className={classes.city} id="outlined"  margin="normal" label="State"   variant="outlined" size = "medium"  onChange = {stateChange} />
        </div>

        <TextField  className={classes.field} id="outlined"  margin="normal" label="Retailer-ID"   variant="outlined" size = "medium" onChange = {retailerIDChange} />

        <TextField  className={classes.field} id="outlined"  margin="normal" label="password"  type="password" variant="outlined" size = "medium" onChange = {passwordChange} />
        <div className = {classes.inroot}>
            <Button className = {classes.userButton} onClick = {Exists} variant="contained" color="secondary">
                Already an user
            </Button>

            <Button className = {classes.signUpButton} onClick = {doSignUp} variant="contained" color="secondary">
                SignUp
            </Button>


       </div>

        </div>

    )



}
