import React from 'react'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import {makeStyles} from "@material-ui/core/styles";
import {useHistory} from "react-router-dom";


const useStyles = makeStyles((theme) =>({
    root:{
        display: 'flex',
        flexDirection: 'row',
        border: 10,
        borderColor: 'black',
        padding: '3rem',
        backgroundColor: 'gray',
        marginLeft: '10rem',
        marginRight: '10rem',
        marginTop: '15rem',

    },
    card1:{
        padding: "3rem",
        width: "10rem"
    },

    card2:{
        marginLeft: "29rem",
        padding:"3rem",
        width: "13rem"
    },

    text:{
        fontSize: "2rem"
    },




}))


export const MainRoute: React.FC = ()=>{
    const classes = useStyles();
    const history = useHistory();


    function goClientLogin(){
        console.log("Client login card is clicked")
        history.push('/Client')
    }

    function goManufacturerLogin(){
        console.log("Manufacturer login card is clickeed")
        history.push('/Manufacturer')
    }

    return(

        <div className={classes.root}>

            <Card  onClick = {goClientLogin} className = {classes.card1}>
                <CardActionArea >
                    <CardContent className = {classes.text} >
                        Retailer
                    </CardContent>
                </CardActionArea>
            </Card>

            <Card  onClick = {goManufacturerLogin}   className = {classes.card2}>
                <CardActionArea >
                    <CardContent className = {classes.text}>
                        Manufacturer
                    </CardContent>
                </CardActionArea>
            </Card>

            </div>
    )
}