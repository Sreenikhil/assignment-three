import React, {useState} from 'react'
import ReactDOM from "react-dom";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Divider from "@material-ui/core/Divider";
import {useHistory, useParams} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { spacing } from '@material-ui/system';
import Axios from 'axios'
import {useSelector} from "react-redux";
import {IRetailerReducer} from "../IRetailerReducer";
import DeleteIcon from '@material-ui/icons/Delete';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';





interface MedicineProps{
    Medicine_ID?: number,
    Medicine_name?: string,
    count?: number,

    setValue?: any,
    val?: number

}

const useStyles =  makeStyles (muiBaseTheme => ({
    card: {
        maxWidth: 450,
        margin: "auto",
        marginTop: '2rem',
        transition: "0.3s",
        boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
        "&:hover": {
            boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
        }
    },
    Button:{
        marginLeft: '1rem'
    },
    icon: {

        fontSize: 25,
        marginLeft: "25rem",
        marginTop: "0.5rem"
    }
}));

export const DisplayMedicine : React.FC<MedicineProps> = (props:MedicineProps) => {
    const retailerId : string | undefined = useSelector<IRetailerReducer,string| undefined>(state => state.RetailerReducer.Retailer_ID)
    const history = useHistory();
    const classes = useStyles();
    let [actualCount, setActualCount] = useState(props.count)

    async function IncFunction() {
        const IncObj = {
            Retailer_ID: retailerId,
            Medicine_ID: props.Medicine_ID,
            value: 1
        }
        const response = await Axios.post('/updateMedicine', IncObj)
        if(response.status === 200){

            setActualCount(actualCount! + 1)
           console.log("I have updated successfully")
        }
        console.log("I am incremented")
    }

    async function DecFunction() {
        const DecObj = {
            Retailer_ID: retailerId,
            Medicine_ID: props.Medicine_ID,
            value: -1
        }
        const response = await Axios.post('/updateMedicine', DecObj)
        if(response.status === 200){
            setActualCount(actualCount! - 1)
            console.log("I have updated successfully")
        }
        console.log("I am decremented")
    }

    async function deleteMed(){
        // Send retailer ID,
        // Medicine ID
        const Delobj = {
            Retailer_ID: retailerId,
            Medicine_ID: props.Medicine_ID
        }

      const response = await Axios.post('/delete', Delobj)
        if(response.status === 200){
            console.log("Item deleted successfully")
        }
        else{
            console.log("Failed to delete item")
        }

        console.log("Delete Medicine is Clicked")
    }


    return (
        <div className="App">
            <Card className={classes.card}>
                <DeleteIcon onClick ={deleteMed} className={classes.icon} />

                <CardContent>
                    <Typography
                        className={"MuiTypography--heading"}
                        variant={"h6"}
                        gutterBottom
                    >
                        Medicine name: {props.Medicine_name}
                    </Typography>
                    <Typography
                        className={"MuiTypography--heading"}
                        variant={"h6"}
                        gutterBottom
                    >
                        Medicine ID: {props.Medicine_ID}
                    </Typography>

                    <Typography
                        className={"MuiTypography--heading"}
                        variant={"h6"}
                        gutterBottom
                    >
                        Count: {actualCount}
                    </Typography>

                    <Button variant="contained" color="primary" onClick={IncFunction}> Increment </Button>

                    <Button variant="contained" color="primary" className={classes.Button}
                            onClick={DecFunction}> Decrement </Button>

                </CardContent>
            </Card>
        </div>
    )
}

/*
icon: {
    margin: theme.spacing.unit,
    fontSize: 32,
  },
<DeleteIcon className={classes.icon} />
       <DeleteForeverIcon className={classes.icon} />
 */