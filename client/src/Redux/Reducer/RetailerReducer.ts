// We will create an enum for default state
import {IRetailer} from "../State/IRetailer";
import {AnyAction} from "redux";
import {ERetailer} from "../Action/RetailerAction";
import {EDestroyer} from "../Action/RetailerAction";

const defaultEnum: IRetailer =  {
    Retailer_ID  : "DEFAULT_RETAILER"
}

// This will typically have a state and an action
export function RetailerReducer(state =  defaultEnum, action: AnyAction){
     switch(action.type){
         case ERetailer.CHANGE_RETAILER:
         return {...state, Retailer_ID: action.Retailer_ID}
         case EDestroyer.DESTROY_RETAILER:
         return  state = defaultEnum
         default:
             return state
     }
}


// There you go!! I have completed creating a reducer for the redux
// At 5:33 PM on a couch in New Jersey on the same day
