"use strict";
exports.__esModule = true;
// Note customMiddleware is my authentication middleware
function customMiddleware(_a) {
    var _b = _a.requireCookie, requireCookie = _b === void 0 ? false : _b;
    console.log("I am a custom middleware");
    console.log("The require cookie value is: " + requireCookie);
    return function middleware_function(request, response, next) {
        var _a;
        console.log("I am inside sub function");
        // Does the signed cookie exists, this is when logged in
        // To test this if condition, we definitely need to set a mock cookie
        if ((_a = request.signedCookies) === null || _a === void 0 ? void 0 : _a.userCookie) {
            console.log("I am in this request");
            console.log("The name of the cookie in middleware is : " + request.signedCookies.userCookie);
            response.locals.userCookie = request.signedCookies.userCookie;
            return next();
        }
        // Cookie does not exists but require cookie is true
        else {
            if (requireCookie) {
                console.log("I am in a requireCookie true");
                console.log("Require cookie is: " + requireCookie);
                var msg = {
                    msge: "Invalid session"
                };
                response.send(msg).status(200);
                return next(new Error('Cookie was required for request but no cookie was found'));
            }
            console.log("I am in a requireCookie true 2");
            // If does not exist we ask to go to the path
            return next();
        }
    };
}
exports.customMiddleware = customMiddleware;
// Middleware for authenticating
// The point of strong middleware is to ensure that all parameters sent through the route handler are valid
// We have included object to get rid of no sql attack
function strongMiddleware(params) {
    return function (request, response, next) {
        var count = 0;
        console.log("I am going through strong middleware");
        var strongParams = {};
        Object.entries(params).forEach(function (_a) {
            var weakParamKey = _a[0], specifiedType = _a[1];
            console.log("The weak param key is: " + request.body[weakParamKey]);
            console.log("I am in for each loop");
            //------------ Checking number type particularly  ------------------------
            if (typeof request.body[weakParamKey] === "number") {
                console.log("I am checking a number");
                console.log("The weak param key is: " + request.body[weakParamKey]);
                if (request.body[weakParamKey] > Number.MAX_VALUE
                    || request.body[weakParamKey] < Number.MIN_VALUE
                    || request.body[weakParamKey] === Infinity
                    || request.body[weakParamKey] === -Infinity) {
                    console.log("There is an exception");
                    return next(new Error("Problem with a number"));
                }
            }
            // I just want to compare but not type check
            if ((request.body != null) &&
                (request.body[weakParamKey] != null) &&
                (request.body[weakParamKey] != undefined) &&
                (typeof request.body[weakParamKey] !== "object") &&
                (request.body[weakParamKey] !== "")) {
                if (typeof request.body[weakParamKey] === specifiedType) {
                    strongParams[weakParamKey] = request.body[weakParamKey];
                }
                else {
                    return next(new Error("Type Mismatch"));
                }
            }
            else
                return next(new Error("Exception occured ........."));
        });
        request.body = null;
        response.locals.strongParams = strongParams;
        return next();
    };
}
exports.strongMiddleware = strongMiddleware;
// Check for negative number in
function abc() {
    return function (request, response, next) {
        console.log("I am a third middleware");
        return next();
    };
}
exports.abc = abc;
