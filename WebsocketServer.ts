
/*
I used websocket to enable communication between retailers and available manufacturers
Retailer sends a message to request the stock of medicines he/she is running out of
Manufacturer checks his production and if available he/she sends the details such as number of days in which he can send the stock,
estimated price etc.
Now the details are available for the retailer to verify

 */

import Websocket = require('ws');
// These are the models that I used to insert communications between client and server
const MedicineRequest = require('./Models/MedicineRequestSchema').MedicineRequest
const MedicineResponse = require('./Models/MedicineResponseSchema').MedicineResponse
const RegularMessages = require('./Models/RegularMessagesSchema').RegularMessages
import mongoose = require('mongoose');


const port: number = 5500;
// This is to instantiate a websocket
const WebsocketServer: Websocket.Server = new Websocket.Server({port})

let myMap = new Map() // map to store all requests


// Connect to the databse

const db_connect =  mongoose.connect("mongodb://localhost:27017/testdb", {useNewUrlParser: true} );




// Now we have to create a route

WebsocketServer.on("connection", async (WebsocketClient: Websocket, req:Request)=>{

    // I am sending a different url from both retailer and manufacturer to differentiate
    // then extracting the word (retailer or manufacturer)
    WebsocketClient.send("You are successfully connected to the server");
    const actorId: string = (req.url).replace('/', "")
    console.log("The actor id is: "+ actorId)
    console.log("The length of the actor id is: "+ actorId.length)

    if(actorId.length === 11){
        console.log("You are a client")
        await RegularMessages.create({Source: "Server", Destination: "Retailer", Message: "You are successfully connected to the server"})
        console.log("A new retailer is connected")
        myMap.set(actorId, WebsocketClient)
    }

    // No matter what, just the length of id is making the difference

    else{
        await RegularMessages.create({Source: "Server", Destination: "Manufacturer", Message: "You are successfully connected to the server"})
        console.log("A new manufacturer is connected")
        console.log("The actor id in the manufacturer chunk is: "+ actorId)
        myMap.set(actorId, WebsocketClient)  // Add to list of manufacturers
    }
    // The real part where, receiving message from client comes on:

    WebsocketClient.on('message', async (message: any)=>{
        console.log('Received message from client: ', message);
        let obj = JSON.parse(message);
        if(obj.id.length === 11){
            console.log("The index of the array is: "+ obj.id)
            // Storing the request in the database and after that sending it to the Manufacturer
            await MedicineRequest.create({
                MedicineCount: obj.MedicineCount,
                MedicineID: obj.MedicineID,
                RetailerID: obj.id,
                Company_name: obj.Company_name,
                Notes: obj.Notes,
                by: "Retailer"
            })

           const iterator = myMap.keys()

            for(let i=0; i<myMap.size; i++){
               const key = iterator.next().value
                console.log(key.length)
                if(key.length === 15){
                    console.log("I have detected a Manufacturer, I will send him the message")
                   const socket: Websocket =  myMap.get(key)
                    if(socket.readyState === Websocket.OPEN){
                        socket.send(message)
                        // Code to delete the response for particular Medicine ID from the server
                        await MedicineResponse.deleteOne({MedicineID: {$eq: obj.MedicineID}})
                    }
                }

            }
        }

        else{
            let obj1 = JSON.parse(message);
            const valid_response = await MedicineRequest.findOne({MedicineID:{$eq: obj1.Medicine_ID}, RetailerID: {$eq: obj1.Retailer_ID}})
            if(valid_response) {
                console.log("The new retailer id is: "+obj1.Retailer_ID)
                // Storing the response in the database
                await MedicineResponse.create({
                    Duration_in_days: obj1.Duration,
                    Cost: obj1.Cost,
                    Note: obj1.Notes,
                    MedicineID: obj1.Medicine_ID,
                    RetailerID: obj1.Retailer_ID,
                    ManufacturerID: obj1.id,
                    by: "Manufacturer"
                })
                   const socket: Websocket = myMap.get(obj1.Retailer_ID)
                   if (socket.readyState === Websocket.OPEN) {
                       socket.send(message)
                       // Code to delete the request for particular Medicine from the database.
                       // This means that request is already served
                       await MedicineRequest.deleteOne({MedicineID: {$eq: obj1.Medicine_ID}})
                   }


            }
        }

    })

        console.log('Broadcasted message to all connected clients!');
});

console.log('Websocket server is up and ready for connections on port', {port});


// logic to send the response to only concerned retailer
// Without retailer id, we have to design a new logic


// Instead of that, I will map, unique id to the concerned retailer.
// Every time, that retailer sends the message he, has to remember the unique id.
// Now at 4:21 PM, If we can create proper front end for Manufacturer, we can see the message sent by the client
