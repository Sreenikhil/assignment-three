import express, {Express, response} from 'express';
import graphqlHTTP from "express-graphql";
import Mongoose from "mongoose";
import GraphQLSchema from "./GraphQLSchema";
import cors from 'cors'
import {customMiddleware} from "../Middlewares/cookie_middleware";


// Instantiate a new Express application (same as we always did)
const app: Express = express();

// Connect to a local 'test' MongoDB
Mongoose.connect('mongodb://localhost:27017/testdb', { useNewUrlParser: true });

// When clients go to '/graphql', they will be able to access our GraphQL endpoint
// Notice how this looks kind of like a middleware, use() is actually a request handler for the whole application
// Here we are using a request handler for the /graphql route only

const corsOptions = {
    origin: 'http://localhost:3000',
    credentials: true,
};

app.use(cors(corsOptions))


app.use('/graphql', graphqlHTTP((req, res) =>(({

    schema: GraphQLSchema,
    context: { req, res },

    graphiql: true

}))
));

app.use(customMiddleware(({requireCookie: true})))



// Have our Express server listen on port 4000
app.listen(4000, () => console.log('Server is up!'));