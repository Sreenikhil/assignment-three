import {GraphQLObjectType, GraphQLString, GraphQLInt} from "graphql"

// Graphql object that will represent our mongoose model
export const GraphQlMedicine = new GraphQLObjectType({
    name: 'Medicine',
    description: 'Medicine of the website',
    // Fields represent what can GraphQl return from the User object
    fields: () => ({

        // There are no resolvers specified for the firstName or lastName field, GraphQL will attempt to get this
        // information bu performing user.firstName and user.lastName respectively, which will work!
        Medicine_name: {type: GraphQLString},
        count: {type: GraphQLInt},
        Medicine_ID: {type: GraphQLInt}

    })
})