import {GraphQLObjectType, GraphQLString, GraphQLInt} from "graphql"

// Graphql object that will represent our mongoose model
export const GraphQlRetailer = new GraphQLObjectType({
    name: 'Retailer',
    description: 'Retailer of the website',
    // Fields represent what can GraphQl return from the User object
    fields: () => ({

        // There are no resolvers specified for the firstName or lastName field, GraphQL will attempt to get this
        // information bu performing user.firstName and user.lastName respectively, which will work!
        Company_name: { type: GraphQLString, resolve: undefined},
        Country_code: {type: GraphQLInt},
        Phone_Number: {type: GraphQLInt},
        Address: { type: GraphQLString },
        Country:{ type: GraphQLString },
        City:{ type: GraphQLString },
        Retailer_ID:{ type: GraphQLString },
        Password:{ type: GraphQLString },

    })
})