import {GraphQLList, GraphQLObjectType, GraphQLString} from "graphql";

import {Retailer} from "../../Models/retailerSchema";
import {GraphQlRetailer} from "../Retailer";
import {RetailerInput} from "../RetailerInput";

import {GraphQlMedicine} from "../Medicine";
import {MedicineInput} from "../MedicineInput";
import {Medicine} from "../../Models/medicineSchema";

import {ICrossReference} from "../../Models/CrossReference";


import {GraphQLCrossReference} from '../CrossReference'
import {CrossReference} from "../../Models/CrossReferenceSchema";
import {Sessions} from "../../Models/SessionSchema";
import {strongMiddleware} from "../../Middlewares/cookie_middleware";
import nanoid = require('nanoid');
import bcrypt = require('bcrypt');





// Root mutations specifies all the ways in which your GraphQl endpoint can create/modify (or mutate) data
export default new GraphQLObjectType({

    name: 'RootMutations',
    description: 'Root level mutator resolvers',

    // Here, fields represents the 'setter functions' you can call with your GraphQl endpoint to 'mutate' various data
    fields: () => ({

        // Specify an endpoint to create a new User and save them in MongoDB
        createRetailer: {

            type:new GraphQLList(GraphQlRetailer),
            description: 'Creates a new user',

            // Here is where we specify those UserInput arguments
            args: {
                input: { type: RetailerInput }
            },


            // How should GraphQL handle creating a user? Save the User with the UserInput args with Mongoose
             resolve: async (parentValue: any, {input : { Company_name, Country_code, Phone_Number, Address, Country, City, Retailer_ID, Password} }) => {
               console.log("The password is: "+ Password)

               // I am storing hash of the password, not the plain text
                await bcrypt.hash(Password, 12, async (err, hash) => {
                    if (err) {
                        console.log("Problem in doing hash" + hash);
                    } else {

                        const retailer_exixst = await Retailer.findOne({$or: [
                                {Company_name: {$eq: Company_name}},
                                {Retailer_ID: {$eq: Retailer_ID}}
                            ]})
                        if(!retailer_exixst) {
                           console.log(hash)
                            Password = hash
                            return await Retailer.create({
                                Company_name,
                                Country_code,
                                Phone_Number,
                                Address,
                                Country,
                                City,
                                Retailer_ID,
                                Password
                            });
                        }

                        }});


            },
        },


        createMedicine: {

            type: GraphQlMedicine,
            description: 'Creates a new medicine',

            // Here is where we specify those UserInput arguments
            args: {
                input: { type: MedicineInput }
            },

            // How should GraphQL handle creating a user? Save the User with the UserInput args with Mongoose
            resolve: (parentValue: any, {input : {Medicine_name, count,  Medicine_ID} }) => {
                return Medicine.create({ Medicine_name,  count, Medicine_ID});
            },
        },




        // At any cost help graphql understand what we want


    })
});