"use strict";
/*
I used websocket to enable communication between retailers and available manufacturers
Retailer sends a message to request the stock of medicines he/she is running out of
Manufacturer checks his production and if available he/she sends the details such as number of days in which he can send the stock,
estimated price etc.
Now the details are available for the retailer to verify

 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Websocket = require("ws");
// These are the models that I used to insert communications between client and server
var MedicineRequest = require('./MedicineRequestSchema').MedicineRequest;
var MedicineResponse = require('./MedicineResponseSchema').MedicineResponse;
var RegularMessages = require('./RegularMessagesSchema').RegularMessages;
var mongoose = require("mongoose");
var port = 5500;
// This is to instantiate a websocket
var WebsocketServer = new Websocket.Server({ port: port });
var Retailer = [];
var Manufacturer = [];
var myMap = new Map(); // map to store all requests
// Connect to the databse
var db_connect = mongoose.connect("mongodb://localhost:27017/testdb", { useNewUrlParser: true });
// Now we have to create a route
WebsocketServer.on("connection", function (WebsocketClient, req) { return __awaiter(void 0, void 0, void 0, function () {
    var actor;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                // I am sending a different url from both retailer and manufacturer to differentiate
                // then extracting the word (retailer or manufacturer)
                WebsocketClient.send("You are successfully connected to the server");
                actor = (req.url).replace('/', "");
                if (!(actor === "retailer")) return [3 /*break*/, 2];
                return [4 /*yield*/, RegularMessages.create({ Source: "Server", Destination: "Retailer", Message: "You are successfully connected to the server" })];
            case 1:
                _a.sent();
                console.log("A new retailer is connected");
                Retailer.push(WebsocketClient); // Add to list of retailers
                _a.label = 2;
            case 2:
                if (!(actor === "manufacturer")) return [3 /*break*/, 4];
                return [4 /*yield*/, RegularMessages.create({ Source: "Server", Destination: "Manufacturer", Message: "You are successfully connected to the server" })];
            case 3:
                _a.sent();
                console.log("A new manufacturer is connected");
                Manufacturer.push(WebsocketClient); // Add to list of manufacturers
                _a.label = 4;
            case 4:
                WebsocketClient.on('message', function (message) { return __awaiter(void 0, void 0, void 0, function () {
                    var obj, obj1, valid_response;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                console.log('Received message from client: ', message);
                                obj = JSON.parse(message);
                                console.log(obj.by);
                                if (!(obj.by === "retailer")) return [3 /*break*/, 2];
                                console.log("The index of the array is: " + obj.Retailerid);
                                // Storing the request in the database
                                return [4 /*yield*/, MedicineRequest.create({ MedicineName: obj.Medname,
                                        MedicineCount: obj.Medcount,
                                        MedicineID: obj.Medid,
                                        RetailerID: obj.Retailerid,
                                        by: obj.by
                                    })
                                    // This is where I am storing the request
                                    // I am basing on the retailer id since it is the ultimate channel where a response from manufacturer has to go
                                ];
                            case 1:
                                // Storing the request in the database
                                _a.sent();
                                // This is where I am storing the request
                                // I am basing on the retailer id since it is the ultimate channel where a response from manufacturer has to go
                                myMap.set(obj.Retailerid, WebsocketClient);
                                // Logic to send message to all manufacturers
                                Manufacturer.forEach(function (client) {
                                    if (client.readyState === Websocket.OPEN) {
                                        client.send(message);
                                    }
                                });
                                _a.label = 2;
                            case 2:
                                if (!(obj.by === "manufacturer")) return [3 /*break*/, 5];
                                obj1 = JSON.parse(message);
                                return [4 /*yield*/, MedicineRequest.findOne({ MedicineID: { $eq: obj1.Medid }, RetailerID: { $eq: obj1.RetailerID } })];
                            case 3:
                                valid_response = _a.sent();
                                if (!valid_response) return [3 /*break*/, 5];
                                console.log("The new retailer id is: " + obj1.RetailerID);
                                // Storing the response in the database
                                return [4 /*yield*/, MedicineResponse.create({
                                        Duration_in_days: obj1.Duration,
                                        Cost: obj1.Cost,
                                        Note: obj1.Note,
                                        MedicineID: obj1.Medid,
                                        RetailerID: obj1.RetailerID,
                                        ManufacturerID: obj1.ManufacturerID,
                                        by: obj1.by
                                    })
                                    // If retailer is ready to listen, then send it to him
                                ];
                            case 4:
                                // Storing the response in the database
                                _a.sent();
                                // If retailer is ready to listen, then send it to him
                                if (myMap.get(obj1.RetailerID).readyState === Websocket.OPEN) {
                                    myMap.get(obj1.RetailerID).send(message);
                                }
                                _a.label = 5;
                            case 5: return [2 /*return*/];
                        }
                    });
                }); });
                console.log('Broadcasted message to all connected clients!');
                return [2 /*return*/];
        }
    });
}); });
console.log('Websocket server is up and ready for connections on port', { port: port });
